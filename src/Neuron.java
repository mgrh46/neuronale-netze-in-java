// From Classic Computer Science Problems in Java Chapter 7
// Copyright 2020 David Kopec
//
// Licensed under the Apache License, Version 2.0

public class Neuron {
    public double[] weights;
    public double outputCache = 0.0;
    public double delta = 0.0;

    /*
     * Bei der Erzeugung wird dem Neuron eine Anzahl zufällig initialisierter Gewichte mitgegeben
     * (Außer es handelt sich um ein Neuron des Inputlayers, dann ist der Wert null)
     */
    public Neuron(double[] weights) {
        this.weights = weights;
    }

    /*
     * Die Funktion nimmt die Outputs des vorgeschalteten Layers oder (wenn es keins gibt) die zu testenden Eingangsdaten
     * Mit den eigenen Gewichten wird eine gewichtete Summe gebildet (aka Kreuzprodukt)
     * Das Kreuzprodukt wird im Outputcache zwischengespeichert (für das spätere Anpassen der Gewichte)
     * Zurückgegeben wird der Wert, der nach dem Aufruf der Aktivierungsfunktion entsteht
     */
    public double output(double[] inputs) {
        outputCache = 0.0;
        for (int i = 0; i < inputs.length; i++) {
            outputCache += inputs[i] * weights[i];
        }
        return Network.sigmoid(outputCache);
    }

    /*
     * Jedes Gewicht wird basierend auf dem Eingangswert, der Lernrate und einem Delta angepasst
     * Das Delta entspricht dem Anteil des Neurons an der Abweichung vom Soll-Ergebnis (aka Schuld am Fehler)
     */
    public void adjustWeights(double[] inputs, double learningRate) {
        for (int w = 0; w < this.weights.length; w++) {
            this.weights[w] += learningRate * inputs[w] * this.delta;
        }
    }
}
