import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

public class Test<T> {
    private double[][] parameter;
    private double[][] classifications;

    /*
     * Das "One-Hot Encoding" übersetzt Klassen in Arrays.
     * Z.B. werden aus den drei Iris-Arten "Iris-setosa", "Iris-versicolor" und "Iris-virginica" die Arrays [1, 0, 0], [0, 1, 0] und [0, 0, 1]
     */
    private double[] oneHotEncoding(T[] labels, T label) {
        double[] oneHotEncoding = new double[labels.length];
        for (int i = 0; i < labels.length; i++) {
            if (labels[i].equals(label)) {
                oneHotEncoding[i] = 1.0;
            } else {
                oneHotEncoding[i] = 0.0;
            }
        }
        return oneHotEncoding;
    }

    /*
     * Die wichtigste Aufgabe der classify-Funktion ist es die Daten in ein Trainings- und Testsatz aufzuteilen
     * In der Regel werden 70% zum Trainieren und 30% zum Testen verwendet
     * Die networkStructure gibt hier erst nur die Anzahl der versteckten Layer und deren Neuronen an
     * In der Funktion wird die Netzstruktur um das Input- und Outputlayer ergänzt
     * Die trainingIterations geben an, ob es eine Mindest- und Höchstzahl an Trainingsdurchläufen gibt
     * Innerhalb des Bereichs, der ohne Angaben bei 0-unendlich liegt, wird das Training nur abgebrochen, wenn der Losswert nicht mehr sinkt
     * Das bedeutet, dass das Netz zwischen zwei Trainingsrunden nichts mehr dazugelernt hat
     * Zum Schluss wird die validate-Funktion aufgerufen, um das Training zu überprüfen
     */
    private void classify(double learningRate, int[] networkStructure, int[] trainingIterations, T[] label) {
        int inputSize = parameter[0].length;
        int outputSize = classifications[0].length;
        int[] structure = new int[networkStructure.length + 2];
        structure[0] = inputSize;
        System.arraycopy(networkStructure, 0, structure, 1, networkStructure.length);
        structure[structure.length - 1] = outputSize;

        Network network = new Network(structure, learningRate);
        double[][] trainParams = new double[(int) (parameter.length * 0.7)][inputSize];
        double[][] trainClassifications = new double[(int) (classifications.length * 0.7)][outputSize];
        double[][] testParams = new double[parameter.length - trainParams.length][inputSize];
        double[][] testClassifications = new double[classifications.length - trainClassifications.length][outputSize];

        System.arraycopy(parameter, 0, trainParams, 0, trainParams.length);
        System.arraycopy(classifications, 0, trainClassifications, 0, trainClassifications.length);
        System.arraycopy(parameter, trainParams.length, testParams, 0, testParams.length);
        System.arraycopy(classifications, trainClassifications.length, testClassifications, 0, testClassifications.length);

        double diff = 1;
        int iterations = 0;
        int[] boundaries = trainingIterations;
        switch (trainingIterations.length) {
            case 0:
                boundaries = new int[]{0, Integer.MAX_VALUE};
                break;
            case 1:
                boundaries = new int[]{trainingIterations[0], Integer.MAX_VALUE};
                break;
        }
        while (((diff > 0.001 || diff < 0) || iterations < boundaries[0]) && iterations < boundaries[1]) {
            diff = network.train(trainParams, trainClassifications);
            iterations++;
        }
        network.validate(testParams, testClassifications, label);
    }

    /*
     * Der Wein-Datensatz besteht aus 178 Datenpunkten mit drei Klassen und 13 Parametern
     * Die Klasse steht an erster Stelle des Datenpunktes
     */
    private void loadWineDataset(T[] label) {
        ArrayList<String[]> wineDataset = loadCSV("ressource/wine.csv");
        Collections.shuffle(wineDataset);
        parameter = new double[wineDataset.size()][];
        classifications = new double[wineDataset.size()][];
        for (int i = 0; i < wineDataset.size(); i++) {
            String[] wine = wineDataset.get(i);
            double[] params = new double[wine.length - 1];
            for (int j = 1; j < wine.length; j++) {
                params[j - 1] = Double.parseDouble(wine[j]);
            }
            parameter[i] = params;
            classifications[i] = oneHotEncoding(label, (T) Integer.valueOf(Integer.parseInt(wine[0])));
        }
        normalize();
    }

    /*
     * Der Iris-Datensatz besteht aus 150 Datenpunkten mit drei Klassen und vier Parametern
     * Die Klasse steht an letzter Stelle des Datenpunktes
     */
    private void loadIrisDataset(T[] label) {
        ArrayList<String[]> irisDataset = loadCSV("ressource/iris.csv");
        Collections.shuffle(irisDataset);
        parameter = new double[irisDataset.size()][];
        classifications = new double[irisDataset.size()][];
        for (int i = 0; i < irisDataset.size(); i++) {
            String[] iris = irisDataset.get(i);
            double[] params = new double[iris.length - 1];
            for (int j = 0; j < iris.length - 1; j++) {
                params[j] = Double.parseDouble(iris[j]);
            }
            parameter[i] = params;
            classifications[i] = oneHotEncoding(label, (T) iris[4]);
        }
        normalize();
    }

    /*
     * Eine Hilfsfunktion, um CSV-Dateien einzulesen und in Datenpunkte und Parameter zu splitten
     */
    private ArrayList<String[]> loadCSV(String file) {
        ArrayList<String[]> lines = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            boolean hasLine = true;
            do {
                String line = bufferedReader.readLine();
                if (line == null) {
                    hasLine = false;
                } else {
                    lines.add(line.split(","));
                }
            } while (hasLine);
        } catch (Throwable t) {
            t.printStackTrace();
            throw new RuntimeException(t.getMessage(), t);
        }
        return lines;
    }

    private void normalize() {
        for (int i = 0; i < parameter[0].length; i++) {
            double max = Double.MIN_VALUE;
            double min = Double.MAX_VALUE;
            for (double[] param : parameter) {
                if (param[i] > max) {
                    max = param[i];
                }
                if (param[i] < min) {
                    min = param[i];
                }
            }
            double diff = max - min;
            for (int j = 0; j < parameter.length; j++) {
                parameter[j][i] = (parameter[j][i] - min) / diff;
            }
        }
    }

    public static void main(String[] args) {
        Integer[] wineLabel = new Integer[]{1, 2, 3};
        String[] irisLabel = new String[]{"Iris-setosa", "Iris-versicolor", "Iris-virginica"};

        Test<Integer> wineTest = new Test<>();
        wineTest.loadWineDataset(wineLabel);
        wineTest.classify(0.2, new int[]{3}, new int[]{10, 150}, wineLabel);

        Test<String> irisTest = new Test<>();
        irisTest.loadIrisDataset(irisLabel);
        irisTest.classify(0.3, new int[]{3}, new int[]{10, 150}, irisLabel);
    }
}
