// From Classic Computer Science Problems in Java Chapter 7
// Copyright 2020 David Kopec
//
// Licensed under the Apache License, Version 2.0

import java.util.Random;

public class Layer {
    public Layer previousLayer = null;
    public final Neuron[] neurons;
    public double[] outputCache;
    public boolean isInput;

    /*
     * Das Inputlayer erhält soviele Neuronen, wie es Parameter in dem Datensatz gibt
     */
    public Layer(int numNeurons) {
        isInput = true;
        neurons = new Neuron[numNeurons];
        for (int i = 0; i < numNeurons; i++) {
            neurons[i] = new Neuron(null);
        }
        outputCache = new double[numNeurons];
    }

    /*
     * Alle weiteren Layer erhalten eine beliebige Anzahl Neuronen, die mit zufällig initialisierten Gewichten bestückt werden.
     * (Außnahme: Das Outputlayer erhält so viele Neuronen, wie es Klassen im Datensatz gibt)
     */
    public Layer(int numNeurons, Layer previousLayer) {
        isInput = false;
        this.previousLayer = previousLayer;
        neurons = new Neuron[numNeurons];
        int inputSize = previousLayer.neurons.length;
        Random random = new Random();
        for (int i = 0; i < numNeurons; i++) {
            double[] randomWeights = random.doubles(inputSize).toArray();
            neurons[i] = new Neuron(randomWeights);
        }
        outputCache = new double[numNeurons];
    }

    /*
     * Wenn es sich um das Inputlayer handelt,so entspricht der Output den Eingangsdaten (die Neuronen sind nur Dummys)
     * Andernfalls wird der Output jedes Neurons berechnet und das Gesamtergebnis zwischengespeichert
     */
    public double[] outputs(double[] inputs) {
        if (this.isInput) {
            outputCache = inputs;
        } else {
            outputCache = new double[neurons.length];
            for (int i = 0; i < neurons.length; i++) {
                outputCache[i] = neurons[i].output(inputs);
            }
        }
        return outputCache;
    }


    /*
     * Die Berechnung des Deltawerts (Anteil am Fehler) ergibt sich nur beim Outputlayer aufgrund der Abweichung zum erwarteten Ergebnis (sog. Groundtruth)
     * Dazu wird die Berechnung "rückwärts" ausgeführt, weswegen die Ableitung der Aktivierungsfunktion notwendig ist
     */
    public void calculateDeltas(double[] expected) {
        for (int i = 0; i < neurons.length; i++) {
            Neuron neuron = neurons[i];
            neuron.delta = Network.derivativeSigmoid(neuron.outputCache) * (expected[i] - outputCache[i]);
        }
    }

    /*
     * Bei allen anderen Layern erfolgt die Berechnung aufgrund der Ergebnisse des nächsten Layers
     * Da jedes Neuron mit jedem Nachfolger verbunden ist, "verschwimmt" mit jeder Schicht der Anteil am Fehler
     *  Dieses Problem nennt man auch "Vanishing Gradients", da irgendwann alle gleich viel "Schuld" sind und nichts daraus lernen können
     */
    public void calculateDeltas(Layer nextLayer) {
        for (int i = 0; i < neurons.length; i++) {
            Neuron neuron = neurons[i];
            double sumWeightsAndDeltas = 0;

            for (Neuron nextNeuron : nextLayer.neurons) {
                double nextDelta = nextNeuron.delta;
                double nextWeight = nextNeuron.weights[i];
                sumWeightsAndDeltas += nextDelta * nextWeight;
            }

            neuron.delta = Network.derivativeSigmoid(neuron.outputCache) * sumWeightsAndDeltas;
        }
    }

    /*
     * Basierend auf der Lernrate und dem vorher ermittelten Delta, werden bei jedem Neuron die Gewichte angepasst
     */
    public void adjustWeights(double learningRate) {
        for (Neuron neuron : neurons) {
            neuron.adjustWeights(previousLayer.outputCache, learningRate);
        }
    }
}
