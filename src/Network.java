// From Classic Computer Science Problems in Java Chapter 7
// Copyright 2020 David Kopec
//
// Licensed under the Apache License, Version 2.0

public class Network {
    private final Layer[] layers;
    public final double learningRate;
    private double lastLoss = 0.0;

    /*
     * Die Sigmoidfunktion dient meistens als Aktivierungsfunktion
     * Daneben gibt es z.B. auch ReLU und andere
     * Bei größeren Netzen variiert die Aktivierungsfunktion je nach Layer und ist nicht immer gleich
     */
    public static double sigmoid(double x) {
        return 1.0 / (1.0 + Math.exp(-x));
    }

    /*
     * Die Ableitung der Funktion wird zum Berechnen der Fehleranteile benötigt
     */
    public static double derivativeSigmoid(double x) {
        double sig = sigmoid(x);
        return sig * (1.0 - sig);
    }

    /*
     * Die Lernrate gibt an, wie stark auf Fehler reagiert werden soll
     * Ist der Wert zu groß, werden optimale Ergebnisse übersprungen, ist er zu klein kann in einem "lokalen Maximum" hängen geblieben werden
     * Die Netzstruktur als int[] gibt an, wie viele Layer mit wie vielen Neuronen es geben soll
     */
    public Network(int[] layerStructure, double learningRate) {
        if (layerStructure.length < 3) {
            throw new IllegalArgumentException("Error: Should be at least 3 layers (1 input, 1 hidden, 1 output).");
        }
        this.learningRate = learningRate;
        layers = new Layer[layerStructure.length];
        layers[0] = new Layer(layerStructure[0]);
        for (int i = 1; i < layerStructure.length; i++) {
            layers[i] = new Layer(layerStructure[i], layers[i - 1]);
        }
    }

    /*
     * Die Funktion reicht die Eingangsdaten und die Aufforderung zum Berechnen an die Layer und damit an die Neuronen durch
     */
    private double[] outputs(double[] input) {
        double[] result = input;
        for (Layer layer : layers) {
            result = layer.outputs(result);
        }
        return result;

    }

    /*
     * Zum Training werden die Testdaten durch das Netz geschickt und dessen Ergebnisse mit den erwarteten Ergebnissen (Groundtruth) verglichen
     * Das Vergleichen führt zu einem Deltawert pro Neuron, der den Anteil am Ergebnis (Korrekt / Fehler) darstellt
     * Mit dem Deltawert und der Lernrate werden die Gewichte aller Neuronen angepasst
     * Zum Ende eines Trainingsdurchlaufs wird ein Referenzwert berechnet, der im besten Fall mit jeder Iteration besser wird
     * Siehe dazu auch die Themen Lossfunktion, Over- und Underfitting
     */
    public double train(double[][] inputs, double[][] expecteds) {
        double[][] batchOut = new double[inputs.length][];
        for (int i = 0; i < inputs.length; i++) {
            double[] xs = inputs[i];
            double[] ys = expecteds[i];
            batchOut[i] = outputs(xs);
            layers[layers.length - 1].calculateDeltas(ys);
            for (int j = layers.length - 2; j >= 0; j--) {
                layers[j].calculateDeltas(layers[j + 1]);
            }
            for (int j = 1; j < layers.length; j++) {
                layers[j].adjustWeights(learningRate);
            }
        }
        double loss = calculateLoss(batchOut, expecteds);
        double lossDiff = lastLoss - loss;
        System.out.println("Loss: " + String.format("%.5f", loss) + " Diff: " + String.format("%.5f", lossDiff));
        lastLoss = loss;
        return lossDiff;
    }

    /*
     * Zum Validieren werden nur die Testdaten durch das Netz verarbeitet und mit den erwarteten Ergebnissen verglichen
     * Es findet keine Anpassung der Gewichte mehr statt!
     * Um unabhängig von der Treffsicherheit des Netzes ein Ergebnis zu ermitteln, wird nur geschaut, welche Klasse es am stärksten vermutet
     * (z.B. könnte das Outputarray [0.8, 0.1, 0.1] lauten, was vereinfach bedeutet, dass es zu 80% die erste Klasse vorhersagt)
     */
    public <T> void validate(double[][] inputs, double[][] expecteds, T[] labels) {
        int correct = 0;
        for (int i = 0; i < inputs.length; i++) {
            double[] input = inputs[i];
            T expected = oneHotDecoding(labels, expecteds[i]);
            T output = oneHotDecoding(labels, outputs(input));

            if (expected.equals(output)) {
                correct++;
            }
        }
        double percentage = (double) correct / (double) inputs.length;
        System.out.println(correct + " correct of " + inputs.length + " = " + percentage * 100 + "%");
    }

    /*
     * Das "One-Hot Decoding" rückübersetzt ein Array in eine Klassen.
     * Z.B. wurden aus den drei Iris-Arten "Iris-setosa", "Iris-versicolor" und "Iris-virginica" die Arrays [1, 0, 0], [0, 1, 0] und [0, 0, 1]
     * Der Outputwert des letzten Layers könnte [0.8, 0.1, 0.1] lauten, was zu "Iris-setosa" führt, da der erste Wert im Array der größte ist
     */
    private <T> T oneHotDecoding(T[] labels, double[] classification) {
        int maxIdx = 0;
        for (int i = 1; i < classification.length; i++) {
            if (classification[i] > classification[maxIdx]) {
                maxIdx = i;
            }
        }
        return labels[maxIdx];
    }

    /*
     * Es gibt viele Metriken, um die Qualität des Trainings einzuschätzen
     * Hier wird die sog. Kreuzentropie als Wert berechnet
     * Dabei werden die berechneten Werte des Netzes mit der Groundtruth verglichen
     * (Der Wert 1e-15 dient nur dazu, dass falls inputs[i][j] == 0 ist das Ergebnis nicht unendlich werden zu lassen)
     */
    private static double calculateLoss(double[][] inputs, double[][] expecteds) {
        double totalLoss = 0.0;

        for (int i = 0; i < inputs.length; i++) {
            double dataPointLoss = 0.0;
            for (int j = 0; j < inputs[i].length; j++) {
                dataPointLoss += expecteds[i][j] * Math.log(inputs[i][j] + 1e-15);
            }
            totalLoss += -dataPointLoss;
        }

        return totalLoss / inputs.length;
    }
}
