# Neuronale Netze in Java

Der hier gezeigte Code basiert auf dem Original von David Kopec aus dem Buch "Classic Computer Science Problems in
Java".
Er ist lizenziert unter der Apache Lizenz Verion 2 (https://www.apache.org/licenses/LICENSE-2.0.html).

Das Projekt wurde von Manuel Groh für die Präsentation an der Werner-von-Siemens-Schule Wetzlar am 23.04.2024
grundlegend überarbeitet und steht unter der gleichen Lizenz zur Verfügung.

## Hinweise zur Lesbarkeit

Um den Code auf den durchschnittlichen Lernstand der 12. Klasse anzupassen, wurde auf die Verwendung der Stram-API
verzichtet.
Gleichzeitig sind so wenig wie möglich Util-Klassen und Funktionen verwendet worden.

Die Daten liegen meist in Form eines zweidimensionalen double-Arrays vor.
Dieses enthält zeilenweise die verschiedenen Datenpunkte und spaltenweise die Eingabeparameter bzw. One-Hot codierten
Erwartungswerte (Groundtruth).

Beispiel aus dem Iris-Datensatz

| input  | [x][0] | [x][1] | [x][2] | [x][3] |
|--------|--------|--------|--------|--------|
| [0][y] | 5.1    | 3.5    | 1.4    | 0.2    |
| [1][y] | 4.9    | 3.0    | 1.4    | 0.2    |
| [2][y] | 4.7    | 3.2    | 1.3    | 0.2    |
| [3][y] | 4.6    | 3.1    | 1.5    | 0.2    |
| [4][y] | 5.0    | 3.6    | 1.4    | 0.2    |
| [5][y] | 5.4    | 3.9    | 1.7    | 0.4    |

| expected | [x][0] | [x][1] | [x][2] |
|----------|--------|--------|--------|
| [0][y]   | 1.0    | 0.0    | 0.0    |
| [1][y]   | 0.0    | 1.0    | 0.0    |
| [2][y]   | 0.0    | 0.0    | 1.0    |

Die Output-Daten sind genauso aufgebaut wie die Groundtruth, jedoch kommt es hier natürlich zu größeren Schwankungen der
Werte.

## Programmablauf

Das Programm verläuft recht linear, der Ablauf ist hier schematisch aufgeführt:

* Test.main
    * erzeugt eine Instanz der Test-Klasse
        * lädt einen Datensatz
        * ruft die classify-Funktion auf
* Test.classify
    * erzeugt eine Instanz der Network-Klasse
    * teilt den Datensatz auf
    * ruft wiederholt Network.train auf
    * ruft am Ende Network.validate auf
* Network.train
    * iteriert über alle Datenpunkte
        * ruft die outputs-Funktion auf (Feed-forward)
        * ruft für jedes Layer (rückwärts) Layer.calculateDeltas auf (Backpropagation)
        * ruft für jedes Layer (vorwärts) Layer.adjustWeights auf
    * berechnet den Loss-Wert des Trainingsdurchgangs
* Network.outputs
    * ruft für jedes Layer Layer.outputs auf
* Network.validate
    * iteriert über alle Datenpunkte
        * ruft die outputs-Funktion auf
        * dekodiert den Vorhersage und den Erwartungswert
        * vergleicht die Vorhersage mit dem Erwartungswert
    * berechnet die Quote korrekter Vorhersagen
* Layer.outputs
    * ruft für jedes Neuron Neuron.output auf, außer es handelt sich um das Inputlayer
* Layer.calculateDeltas
    * Iteriert über alle Neuronen
        * (Falls Input- oder Hiddenlayer) berechnet das Kreuzprodukt aus den Gewichten und Deltas der nachfolgend
          verbundenen Neuronen
        * (Falls Outputlayer) berechnet die Differenz aus Vorhersage und Erwartungswert
        * berechnet das Delta als Produkt aus dem Kreuzprodukt bzw. der Differenz und der Ableitung des eigenen Outputs
* Layer.adjustWeights
    * ruft für jedes Neuron Neuron.adjustWeights auf
* Neuron.output
    * berechnet das Kreuzprodukt aus den Eingabewerten und den Gewichten
    * berechnet den Ausgabewert als Ergebnis der Sigmoidfunktion mit dem Kreuzprodukt
* Neuron.adjustWeight
    * berechnet die Summe aus den Gewichten mit den Produkten aus Eingabewerten, Lernrate und Deltawerten

